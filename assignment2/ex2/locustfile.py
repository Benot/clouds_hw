import time
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    host = "http://137.135.186.138"

    @task
    def hello_world(self):
        self.client.get("/numericalintegralservice/0/3.14159")
