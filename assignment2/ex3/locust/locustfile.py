import time
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    host = "http://webapphw2ex3.azurewebsites.net"

    @task
    def hello_world(self):
        self.client.get("/numericalintegralservice/0/3.14159")
