import matplotlib.pyplot as plt
import pandas as pd

# Load data
data_local = pd.read_csv('../ex1_3/plot_ex1_3_stats_history.csv')
data_scaleset = pd.read_csv('../ex2/plot_ex2_stats_history.csv')
data_webapp = pd.read_csv('../ex3/locust/plot_ex3_very_long_stats_history.csv')
data_function = pd.read_csv('../ex4/locust/plot_ex4_stats_history.csv')

# Plot
plt.figure(figsize=(6.8, 4.2))
x = range(len(data_local['Timestamp']))
x_len = data_local['Requests/s'].shape[0]
plt.plot(x, data_local['Requests/s'] - data_local['Failures/s'])
plt.plot(x, data_scaleset['Requests/s'] - data_scaleset['Failures/s'])
plt.plot(x, data_webapp['Requests/s'][:x_len] - data_webapp['Failures/s'][:x_len])
plt.plot(x, data_function['Requests/s'] - data_function['Failures/s'])
plt.title('Comparison of number of successful requests in 3 minutes')
plt.xlabel('Time (s)')
plt.ylabel('nb successful requests per second')
plt.legend(["locally deployed", "VM scaleset (2 VMs)", "autoscale webapp (max 3 instances)", "autoscale serverless function"])
plt.savefig("recap_graph.png")
plt.close()

plt.figure(figsize=(6.8, 4.2))
x = range(len(data_webapp['Timestamp']))
plt.plot(x, data_webapp['Requests/s'] - data_webapp['Failures/s'])
plt.plot(x, data_webapp['Failures/s'])
plt.title('Complete webapp runtime (12 minutes)')
plt.xlabel('Time (s)')
plt.ylabel('nb requests per second')
plt.legend(["successful", "failures"])
plt.savefig("webapp_comlete.png")
plt.close()
