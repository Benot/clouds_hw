#!/usr/bin/python
"""
Answer to question 1.1

usage: num_integration.py [-h] [--lower LOWER] [--upper UPPER] [--verbose [VERBOSE]]

by Benoît Knott
"""
import argparse
from math import sin

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def black_box_function(x):
    return abs(sin(x))

def num_integration(lower, upper, N):
    dx = (upper - lower) / N
    integral = 0.0

    for i in range(N):
        xip12 = dx * (i + 0.5)
        dI = black_box_function(xip12) * dx
        integral += dI

    return integral

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--lower', '-l', type=int, help='Lower limit on the interval to compute the integral on (default: 0)', default=0)
    parser.add_argument('--upper', '-u', type=int, help='Upper limit on the interval to compute the integral on (default: 3.14159)', default=3.14159)
    parser.add_argument('--verbose', '-v', type=str2bool, nargs='?', const=True, help='Verbose mode', default=True)
    args = parser.parse_args()

    if args.verbose:
        print("Computing the interval of the black box function between", args.lower, "and", args.upper, ".")

    for N in [10, 100, 1000, 10000, 100000, 1000000]:
        result = num_integration(args.lower, args.upper, N)
        if args.verbose:
            print("The result for N=", N, " is ", result, sep="")

if __name__== "__main__":
    main()  
