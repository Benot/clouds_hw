"""
Reimplementation of the numerical implementation to be used by falsk.

by Benoît Knott
"""
from math import sin
# import datetime

def black_box_function(x):
    return abs(sin(x))

def num_integration(lower, upper, N):
    # a = datetime.datetime.now()

    dx = (upper - lower) / N
    integral = 0.0

    for i in range(N):
        xip12 = dx * (i + 0.5)
        dI = black_box_function(xip12) * dx
        integral += dI

    # b = datetime.datetime.now()
    # print(b-a)

    return integral
