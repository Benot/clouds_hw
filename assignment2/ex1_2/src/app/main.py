"""
Answer to question 1.2

by Benoît Knott
"""
from flask import Flask, request
from mypkg.num_integration import num_integration
            
app = Flask(__name__)


def num_integral_service(lower, upper):
    lower = float(lower)
    upper = float(upper)

    ret = ('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">\n'
           '<html>\n'
           '<head>\n'
           '   <title>numerical integration</title>\n'
           '</head>\n'
           '<body>\n')

    ret += "   <p>Computing the interval of the black box function between " + str(lower) + " and " + str(upper) + ".</p>\n"

    ret += "   <p>"
    for N in [10, 100, 1000, 10000, 100000, 1000000]:
        result = num_integration(lower, upper, N)
        ret += "   The result for N=" + str(N) + " is " + str(result) + "<br>\n"
    ret += "   </p>\n"

    ret += ('</body>\n'
            '</html>\n')
    return ret

app.add_url_rule('/numericalintegralservice/<lower>/<upper>', 'num_integral_service', num_integral_service)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

