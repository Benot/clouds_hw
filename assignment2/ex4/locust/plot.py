import matplotlib.pyplot as plt
import pandas as pd

# Load data
data = pd.read_csv('plot_ex4_stats_history.csv')

# Plot
plt.figure(figsize=(6.8, 4.2))
x = range(len(data['Timestamp']))
plt.plot(x, data['Requests/s'] - data['Failures/s'])
plt.plot(x, data['Failures/s'])
# plt.xticks(x, data['Timestamp'] - data['Timestamp'][0])
plt.xlabel('Time (s)')
plt.ylabel('nb requests per second')
plt.legend(["successful", "failures"])
plt.show()
