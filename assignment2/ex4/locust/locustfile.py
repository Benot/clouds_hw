import time
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    host = "https://numericalintegralservicebenoitdjknott.azurewebsites.net"

    @task
    def hello_world(self):
        self.client.get("/numericalintegralservice/0/3.14159")
