# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
from typing import List

def main(mapOutput: List[dict]) -> dict:
    reduce_inputs = {}

    for pair in mapOutput:
        key = list(pair.keys())[0]
        value = list(pair.values())[0]
        if key in reduce_inputs:
            reduce_inputs[key].append(value)
        else:
            reduce_inputs[key] = [value]
    
    # transform reduce_inputs into a list of key-value pairs
    reduce_inputs_copy = reduce_inputs
    reduce_inputs = []
    for key, value in reduce_inputs_copy.items():
        reduce_inputs.append({key: value})

    return reduce_inputs
