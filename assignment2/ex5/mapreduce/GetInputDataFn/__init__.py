# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
from azure.storage.blob import BlobServiceClient
from typing import List, Dict

def main(fileNames: List[str]) -> List[Dict[int, str]]:
    connect_str = "DefaultEndpointsProtocol=https;AccountName=benoitdjknottmapreduce;AccountKey=8WnslN6sp7y+9m2D4+ArOLm4P3HT0Zpu336zJFAPUkZYdGTLw4IT1BE4BmPAtw/RARI8UAqW9VUg+AStaDRNMw==;EndpointSuffix=core.windows.net"

    blob_service_client = BlobServiceClient.from_connection_string(connect_str)
    container_client = blob_service_client.get_container_client("containerblobstorehw2ex5")
    
    map_input = []
    line_nb = 1
    for name in fileNames:
        blob = container_client.download_blob(name).readall()
        for line in blob.decode("utf8").split("\r\n"):
            map_input.append({line_nb: line})
            line_nb += 1

    return map_input
