# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json

import azure.functions as func
import azure.durable_functions as df


def orchestrator_function(context: df.DurableOrchestrationContext):
    
    # To use a dummy input (not from blobstore)
    # # https://youtu.be/2Hoj3OXD0E8 :)
    # dummy_input = [{1: "I always thought I might be bad"},
    #                {2: "Now I'm sure that it's true"},
    #                {3: "'Cause I think you're so good"},
    #                {4: "And I'm nothing like you"},
    #                {5: "Look at you go"},
    #                {6: "I just adore you"},
    #                {7: "I wish that I knew"},
    #                {8: "What makes you think I'm so special"}]

    # If you want only one file you still need an array of one element as input
    blob_input = yield context.call_activity("GetInputDataFn", ["mrinput-1.txt", "mrinput-2.txt", "mrinput-3.txt", "mrinput-4.txt"])

    # Could go into more details for the verification of the input but that's
    # not the point here...
    if not isinstance(blob_input, list):
        raise Exception("A list is required as input")
    for element in blob_input:
        if not isinstance(element, dict):
            raise Exception("A list of dictionary is required as input")
    
    # logging.info("verification ok")

    # ============== Map ==============
    tasks = []
    for pair in blob_input:
        tasks.append(context.call_activity("Mapper", pair))

    map_output = yield context.task_all(tasks)
    
    # logging.info(f"map output is: {map_output}")

    # Turn the map output into a 1D list of key-value pairs
    map_output = [word for line in map_output for word in line]

    # ============== Shuffle ==============
    shuffle_output = yield context.call_activity("Shuffler", map_output)

    # logging.info(f"map output is: {shuffle_output}")

    # ============== Reduce ==============
    tasks = []
    for pair in shuffle_output:
        tasks.append(context.call_activity("Reducer", pair))
    
    reducer_output = yield context.task_all(tasks)

    # logging.info(f"reducer output is: {reducer_output}")

    return reducer_output

main = df.Orchestrator.create(orchestrator_function)