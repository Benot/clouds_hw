# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
from typing import List

def main(offsetLinePair: dict) -> List[dict]:
    word_count_pairs = []

    for word in list(offsetLinePair.values())[0].split():
        word_count_pairs.append({word: 1})

    return word_count_pairs
