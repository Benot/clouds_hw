# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging


def main(wordListPair: dict) -> dict:
    sum = 0

    for count in list(wordListPair.values())[0]:
        sum += count
    
    return {list(wordListPair.keys())[0]: sum}
